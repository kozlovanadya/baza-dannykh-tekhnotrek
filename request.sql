-- Показать последнее сообщение от данного пользователя
CREATE OR REPLACE FUNCTION
last_message_by_author(a_id INTEGER) RETURNS TEXT AS $$
DECLARE
BEGIN

  RETURN (SELECT text
          FROM message_
          WHERE author_id = a_id
          ORDER BY time_updating DESC
          LIMIT 1
  );

END;
$$ LANGUAGE plpgsql VOLATILE;


-- Показать последние сообщения от данного пользователя
CREATE OR REPLACE FUNCTION
last_messages_by_author(a_id INTEGER) RETURNS SETOF TEXT AS $$
DECLARE
BEGIN

  RETURN QUERY (SELECT text
          FROM message_
          WHERE author_id = a_id
          ORDER BY time_updating DESC
  );

END;
$$ LANGUAGE plpgsql VOLATILE;


-- Показать всю переписку по данной теме
CREATE OR REPLACE FUNCTION
messages_on_theme(t_id INTEGER) RETURNS SETOF TEXT AS $$
DECLARE
BEGIN

  RETURN QUERY (SELECT text
          FROM message_
          WHERE theme_id = t_id
          ORDER BY time_updating DESC
  );

END;
$$ LANGUAGE plpgsql VOLATILE;


--Зарегестрировать нового пользователя
CREATE OR REPLACE FUNCTION
login_new_user(log VARCHAR(255), pass VARCHAR(255), email VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL,
  surname VARCHAR(255) DEFAULT NULL, middle_name VARCHAR(255) DEFAULT NULL,
  birthday DATE DEFAULT NULL, location varchar(50) DEFAULT NULL, country varchar(100) DEFAULT NULL) RETURNS VOID AS $$
DECLARE
BEGIN

  if ((SELECT count(login)
      FROM person
      WHERE login = log
      ) > 0)
    THEN RETURN;
    ELSE   INSERT INTO person (login, password, email,
                      name, surname, middle_name, birthday,
                      registration_day, location, country)
           VALUES (log, pass, email, name, surname, middle_name, birthday, NOW(), location, country);
  END IF;

END;
$$ LANGUAGE plpgsql VOLATILE;


--Добавление сообщения
CREATE OR REPLACE FUNCTION
create_message(text TEXT, a_id INT, t_id INT) RETURNS VOID AS $$
DECLARE
BEGIN

  INSERT INTO message_ (text, author_id, theme_id, time_creating, time_updating)
  VALUES (text, a_id, t_id, NOW(), NOW()
  );

  UPDATE theme SET time_updating = NOW()
  WHERE theme_id = t_id;

END;
$$ LANGUAGE plpgsql VOLATILE;


--Исправление данного сообщения
CREATE OR REPLACE FUNCTION
update_message(t TEXT, m_id INT) RETURNS VOID AS $$
DECLARE
BEGIN

  UPDATE message_ SET text = t, time_updating = NOW()
  WHERE message_id = m_id;

  UPDATE theme SET time_updating = NOW()
  WHERE theme_id = (
    SELECT theme_id
    FROM message_
    WHERE message_id = m_id
  );

END;
$$ LANGUAGE plpgsql VOLATILE;


--Показать собеседников по данной теме
CREATE OR REPLACE FUNCTION
friends_this_theme(t_id INTEGER) RETURNS SETOF VARCHAR(255) AS $$
DECLARE
BEGIN

  RETURN QUERY (SELECT login
          FROM person
          JOIN message_
          ON author_id = person_id
          WHERE theme_id = t_id
  );

END;
$$ LANGUAGE plpgsql VOLATILE;


--Переименование темы
CREATE OR REPLACE FUNCTION
update_theme(title TEXT, t_id INT) RETURNS VOID AS $$
DECLARE
BEGIN

  UPDATE theme SET title = title, time_updating = NOW()
  WHERE theme_id = t_id;

END;
$$ LANGUAGE plpgsql VOLATILE;


--Создать лайк или изменить его оценку
CREATE OR REPLACE FUNCTION
change_or_create_like(m_id INTEGER, a_id INTEGER) RETURNS VOID AS $$
DECLARE
  id INTEGER;
BEGIN
  id = (SELECT grade
        FROM like_ l
        WHERE l.author_id = a_id AND l.message_id = m_id
  );

  IF NOT(id is NULL)
    THEN
      UPDATE like_ l
      SET grade = (1 - grade)
      WHERE l.author_id = a_id AND l.message_id = m_id;
    ELSE
      INSERT INTO like_ (message_id, author_id, grade)
      VALUES (m_id, a_id, 1);
  END IF;

END;
$$ LANGUAGE plpgsql VOLATILE;


--Показать все сообщения: автор, тема и сам текст
CREATE OR REPLACE VIEW show_all_messages AS (
    SELECT login, title, text
    FROM message_ m
    JOIN person p
    ON p.person_id = m.author_id
    JOIN theme t
    ON t.theme_id = m.theme_id
);


--Показать последние 10 сообщений: автор, тема, время создания и сам текст
CREATE OR REPLACE VIEW show_top10_messages AS (
    SELECT login, title, m.time_creating, text
    FROM message_ m
    JOIN person p
    ON p.person_id = m.author_id
    JOIN theme t
    ON t.theme_id = m.theme_id
    ORDER BY m.time_creating DESC
    LIMIT 10
);